from tqdm import tqdm
from numpy import save
from src.objects.game import Game
import argparse
import random

NUM_GAMES = 1000

TRAIN_TEST = "train"

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--type', '-t', choices=['train', 'test', 'none'], default='none')
    parser.add_argument('--games', '-g', type=int, default=1)
    parser.add_argument('--verbose', '-v', action='store_true')
    parser.add_argument('--seed', '-s', type=int, default=1234)
    args = parser.parse_args()
    if args.seed:
        random.seed(args.seed)
    game = Game(args.verbose)
    for _ in (tqdm(range(args.games)) if not args.verbose else range(args.games)):
        game.play()
        game.init_players()

    if args.type != 'none':
        save(args.type + '_data', game.training_data)
        save(args.type + '_gt', game.training_gt)
