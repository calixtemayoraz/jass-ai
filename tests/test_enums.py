import unittest

from src.objects.enums import *


class EnumsTest(unittest.TestCase):

    def test_suit_and_value(self):
        """just testing the enums to make sure everything works fine..."""
        s1 = Suit.Pique
        s2 = Suit.Trefle
        s3 = Suit.Pique
        v1 = Value.Six
        v2 = Value.As
        v3 = Value.Six
        with self.subTest("equal suits"):
            self.assertEqual(s1, s3)
        with self.subTest("unequal suits"):
            self.assertNotEqual(s1, s2)
        with self.subTest("equal values"):
            self.assertEqual(v1, v3)
        with self.subTest("unequal values"):
            self.assertNotEqual(v1, v2)

    def test_who(self):
        assert len(Who) in (4, 6)
        with self.subTest("teams"):
            if len(Who) == 4:
                self.assertEqual(Who.teams(),
                                 ([Who.Alice, Who.Carol],
                                  [Who.Bobby, Who.David]))
            else:
                self.assertEqual(Who.teams,
                                 ([Who.Alice, Who.Carol, Who.Emily],
                                  [Who.Bobby, Who.David, Who.Frank]))
        with self.subTest("__str__"):
            self.assertEqual(str(Who.Alice), "Alice")
        with self.subTest("next"):
            self.assertEqual(Who.Alice.next(), Who.Bobby)
            self.assertEqual(Who.Alice.next(2), Who.Carol)
            if len(Who) == 4:
                self.assertEqual(Who.Alice.next(-1), Who.David)
            else:
                self.assertEqual(Who.Alice.next(-1), Who.Frank)
        with self.subTest("left"):
            self.assertEqual(Who.Bobby.left, Who.Alice)
        with self.subTest("right"):
            self.assertEqual(Who.Bobby.right, Who.Carol)
        with self.subTest("all"):
            if len(Who) == 4:
                self.assertEqual(Who.Carol.all,
                                 [Who.Carol, Who.David, Who.Alice, Who.Bobby])
            else:
                self.assertEqual(Who.Carol.all,
                                 [Who.Carol, Who.David, Who.Emily,
                                  Who.Frank, Who.Alice, Who.Bobby])
        with self.subTest("others"):
            if len(Who) == 4:
                self.assertEqual(Who.David.others,
                                 [Who.Alice, Who.Bobby, Who.Carol])
            else:
                self.assertEqual(Who.David.others,
                                 [Who.Emily, Who.Frank,
                                  Who.Alice, Who.Bobby, Who.Carol])
        with self.subTest("team"):
            if len(Who) == 4:
                self.assertEqual(Who.Alice.team,
                                 [Who.Alice, Who.Carol])
            else:
                self.assertEqual(Who.Alice.team,
                                 [Who.Alice, Who.Carol, Who.Emily])
        with self.subTest("mates"):
            if len(Who) == 4:
                self.assertEqual(Who.Alice.mates, [Who.Carol])
            else:
                self.assertEqual(Who.Alice.mates, [Who.Carol, Who.Emily])
        with self.subTest("opponents"):
            if len(Who) == 4:
                self.assertEqual(Who.Bobby.opponents,
                                 [Who.Carol, Who.Alice])
            else:
                self.assertEqual(Who.Bobby.opponents,
                                 [Who.Carol, Who.Emily, Who.Alice])


if __name__ == '__main__':
    unittest.main()
