import unittest
from random import choice

from src.objects.card import *


class CardTest(unittest.TestCase):
    def test_card_comparison(self):
        atout = Suit.Pique
        bourg = Card(Value.Valet, Suit.Pique)
        nel = Card(Value.Neuf, Suit.Pique)
        as_atout = Card(Value.As, Suit.Pique)
        as_coeur = Card(Value.As, Suit.Coeur)
        roi_coeur = Card(Value.Roi, Suit.Coeur)
        any_other = Card(choice([v for v in Value]), Suit.Carreau)
        any_atout = Card(choice([v for v in Value]), Suit.Pique)

        def assert_first_wins(card1, card2):
            self.assertEqual(card1, Card.compare_cards(card1, card2, atout))

        with self.subTest("bourg over other"):
            assert_first_wins(bourg, any_other)
        with self.subTest("bourg over nel"):
            assert_first_wins(bourg, nel)
        with self.subTest("nel over as atout"):
            assert_first_wins(nel, as_atout)
        with self.subTest("as over other"):
            assert_first_wins(as_coeur, roi_coeur)
        with self.subTest("color refusal"):
            assert_first_wins(any_other, as_coeur)
        with self.subTest("other isn't atout"):
            assert_first_wins(any_atout, any_other)
        with self.subTest("color cut"):
            self.assertEqual(as_atout, Card.compare_cards(any_other, as_atout, atout))

    def test_export_14(self):
        with self.subTest("with atout"):
            np.testing.assert_equal(
                Card(Value.Dix, Suit.Carreau).export_14(Suit.Carreau),
                np.array([1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0])
            )
        with self.subTest("without atout"):
            np.testing.assert_equal(
                Card(Value.Dix, Suit.Carreau).export_14(),
                np.array([0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0])
            )

    def test_export_36(self):
        with self.subTest("with atout"):
            np.testing.assert_equal(
                Card(Value.Dix, Suit.Carreau).export_36(Suit.Carreau),
                np.array([
                    0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 2, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0
                ])
            )
        with self.subTest("without atout"):
            np.testing.assert_equal(
                Card(Value.Dix, Suit.Carreau).export_36(),
                np.array([
                    0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 1, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0
                ])
            )

    def test_from_encoding(self):
        card = Card(choice([v for v in Value]), choice(list(Suit)))
        with self.subTest("with atout"):
            self.assertEqual(card, Card.from_14_encode(card.export_14(card.suit)))
        with self.subTest("without atout"):
            self.assertEqual(card, Card.from_14_encode(card.export_14()))


if __name__ == '__main__':
    unittest.main()
