import unittest

import numpy as np

from src.objects import cards_distribution as cd
from src.objects.card import Card
from src.objects.card_coll import Hand, Mat
from src.objects.enums import Suit, Value, Who
from src.objects.player import Player


class HelperFunctionsTest(unittest.TestCase):

    def test_check_double_stochasticity(self):
        self.assertTrue(cd._is_doubly_stochastic(np.array(
            [[0.2, 1.0, 0.1, 0.3, 0.4],
             [0.8, 0.0, 0.9, 0.7, 0.6]]
        )))
        self.assertTrue(cd._is_doubly_stochastic(np.array(
            [[0, 0],
             [1, 1]]
        )))
        self.assertFalse(cd._is_doubly_stochastic(np.array(
            [[0, 1],
             [0, 1]]
        )))

    def test_make_doubly_stochastic(self):
        with self.assertRaises(ValueError):
            cd._make_doubly_stochastic(np.random.uniform(high=10, size=(4, 36)),
                                       row_sums=np.array([9, 9, 9, 10]))
        self.assertTrue(np.allclose(
            cd._make_doubly_stochastic(np.array(
                [[2, 10, 1, 3, 4],
                 [8, 0, 9, 7, 6]]
            )),
            np.array(
                [[0.2, 1.0, 0.1, 0.3, 0.4],
                 [0.8, 0.0, 0.9, 0.7, 0.6]]
            )
        ))
        self.assertTrue(cd._is_doubly_stochastic(cd._make_doubly_stochastic(
            np.random.uniform(high=10, size=(4, 36)))))
        self.assertTrue(cd._is_doubly_stochastic(cd._make_doubly_stochastic(
            np.random.uniform(high=10, size=(4, 36)),
            row_sums=np.array([0, 10, 12, 14])
        )))

    def test_sample_from_categorical_distribution(self):
        with self.assertRaises(ValueError):
            cd._sample_from_categorical_distribution(np.array([0.5, 0.6]), 1)
        with self.assertRaises(ValueError):
            cd._sample_from_categorical_distribution(np.array([0.1, 0.9]), 0)

        self.assertEqual(
            cd._sample_from_categorical_distribution(
                p=np.array([0.0, 1.0, 0.0, 0.0]), n=1)[0], 1)
        sample_size = 10000
        margin = 0.1
        sample = cd._sample_from_categorical_distribution(
            p=np.array([0.0, 0.2, 0.8, 0.0]), n=sample_size)
        self.assertTrue(isinstance(sample, np.ndarray))
        self.assertEqual(sample.shape, (sample_size,))
        self.assertAlmostEqual(np.sum(sample == 1) / sample_size, 0.2,
                               delta=margin)
        self.assertAlmostEqual(np.sum(sample == 2) / sample_size, 0.8,
                               delta=margin)

    def test_sample_hands(self):
        # If all hands are known, returns a single sample.
        self.assertTrue(np.allclose(
            cd._sample_hands(p=np.array(
                [[0, 0],
                 [0, 1],
                 [1, 0]]
            ), n=10),
            np.array([[2, 1]])
        ))
        p = np.array(
            [[0.2, 1.0, 0.1, 0.3, 0.4],
             [0.6, 0.0, 0.5, 0.7, 0.2],
             [0.2, 0.0, 0.4, 0.0, 0.4]]
        )
        n = 1000
        samples = cd._sample_hands(p=p, n=n)
        self.assertTrue(isinstance(samples[0, 0], np.int32))
        self.assertTrue(samples.min() == 0)
        self.assertTrue(samples.max() == 2)
        self.assertTrue(samples[:, 1].max() == 0)
        self.assertTrue(samples[:, 3].max() == 1)
        r_sums = p.sum(axis=1)
        for i in range(3):
            self.assertEqual(np.argwhere(samples == i).shape[0], r_sums[i] * n)


class CardsDistributionTest(unittest.TestCase):
    def init_player(self, who):
        return Player(hand=Hand(np.vstack([
            Card(Value.Six, Suit.Pique).export_14(),
            Card(Value.Dix, Suit.Pique).export_14(),
            Card(Value.Huit, Suit.Trefle).export_14(),
            Card(Value.Valet, Suit.Trefle).export_14(),
            Card(Value.Dame, Suit.Trefle).export_14(),
            Card(Value.As, Suit.Trefle).export_14(),
            Card(Value.Valet, Suit.Carreau).export_14(),
            Card(Value.Neuf, Suit.Coeur).export_14(),
            Card(Value.Roi, Suit.Coeur).export_14(),
        ])), who=who)

    def test_init(self):
        if len(Who) != 4:
            raise NotImplementedError('Tests only for 4 players case.')
        me = self.init_player(Who.Carol)
        my_view = cd.CardsDistribution(me)
        self.assertEqual(my_view.me, Who.Carol)
        self.assertIsNone(my_view.atout)
        self.assertIsNone(my_view.atout_picker)
        self.assertTrue(np.all(my_view.num_cards == np.array([9, 9, 9, 9])))
        t = 1 / 3  # 9 / 27
        self.assertTrue(
            np.allclose(my_view.p,
                        np.array([
                            # Alice:
                            [0, t, t, t, 0, t, t, t, t,   # Pique
                             t, t, t, 0, t, t, t, 0, t,   # Coeur
                             t, t, t, t, t, 0, t, t, t,   # Carreau
                             t, t, 0, t, t, 0, 0, t, 0],  # Trefle
                            # Bobby
                            [0, t, t, t, 0, t, t, t, t,   # Pique
                             t, t, t, 0, t, t, t, 0, t,   # Coeur
                             t, t, t, t, t, 0, t, t, t,   # Carreau
                             t, t, 0, t, t, 0, 0, t, 0],  # Trefle
                            # Carol
                            [1, 0, 0, 0, 1, 0, 0, 0, 0,   # Pique
                             0, 0, 0, 1, 0, 0, 0, 1, 0,   # Coeur
                             0, 0, 0, 0, 0, 1, 0, 0, 0,   # Carreau
                             0, 0, 1, 0, 0, 1, 1, 0, 1],  # Trefle
                            # David
                            [0, t, t, t, 0, t, t, t, t,   # Pique
                             t, t, t, 0, t, t, t, 0, t,   # Coeur
                             t, t, t, t, t, 0, t, t, t,   # Carreau
                             t, t, 0, t, t, 0, 0, t, 0]   # Trefle
                        ])))
        my_view.set_atout(Suit.Carreau, Who.Alice)
        self.assertEqual(my_view.atout, Suit.Carreau)

    def test_set_atout(self):
        if len(Who) != 4:
            raise NotImplementedError('Tests only for 4 players case.')
        me = self.init_player(Who.Carol)
        my_view = cd.CardsDistribution(me)
        # Alice announce Carreau atout
        atout = Suit.Carreau
        my_view.set_atout(atout, Who.Alice)
        self.assertEqual(my_view.atout, atout)
        self.assertEqual(my_view.atout_picker, Who.Alice)
        # For now, we have not implemented any twist of the cards probabilities
        # based on the atout and who set it.

    def test_play(self):
        if len(Who) != 4:
            raise NotImplementedError('Tests only for 4 players case.')
        me = self.init_player(Who.Carol)
        my_view = cd.CardsDistribution(me)
        # Alice announce Carreau atout
        atout = Suit.Carreau
        my_view.set_atout(atout, Who.Alice)
        # Initialize the mat
        mat = Mat(atout, Who.Alice)
        # Alice plays Neuf de Carreau
        mat.play(Card(Value.Neuf, Suit.Carreau))
        my_view.play(mat=mat, by=Who.Alice)
        self.assertTrue(np.all(my_view.num_cards == np.array([8, 9, 9, 9])))
        t = 9 / 26
        u = 8 / 26
        self.assertTrue(
            np.allclose(my_view.p,
                        np.array([
                            # Alice:
                            [0, u, u, u, 0, u, u, u, u,   # Pique
                             u, u, u, 0, u, u, u, 0, u,   # Coeur
                             u, u, u, 0, u, 0, u, u, u,   # Carreau
                             u, u, 0, u, u, 0, 0, u, 0],  # Trefle
                            # Bobby
                            [0, t, t, t, 0, t, t, t, t,   # Pique
                             t, t, t, 0, t, t, t, 0, t,   # Coeur
                             t, t, t, 0, t, 0, t, t, t,   # Carreau
                             t, t, 0, t, t, 0, 0, t, 0],  # Trefle
                            # Carol
                            [1, 0, 0, 0, 1, 0, 0, 0, 0,   # Pique
                             0, 0, 0, 1, 0, 0, 0, 1, 0,   # Coeur
                             0, 0, 0, 0, 0, 1, 0, 0, 0,   # Carreau
                             0, 0, 1, 0, 0, 1, 1, 0, 1],  # Trefle
                            # David
                            [0, t, t, t, 0, t, t, t, t,   # Pique
                             t, t, t, 0, t, t, t, 0, t,   # Coeur
                             t, t, t, 0, t, 0, t, t, t,   # Carreau
                             t, t, 0, t, t, 0, 0, t, 0]   # Trefle
                        ])))
        # Bobby plays Sept of Pique
        # This shows that he has either no atout (since I have the Valet).
        mat.play(Card(Value.Sept, Suit.Pique))
        my_view.play(mat=mat, by=Who.Bobby)
        self.assertTrue(np.all(my_view.num_cards == np.array([8, 8, 9, 9])))
        self.assertTrue(
            np.all(my_view.p[Who.Bobby.value, 18:27] == np.zeros(9)))

    def test_play_refuse_atout(self):
        if len(Who) != 4:
            raise NotImplementedError('Tests only for 4 players case.')
        me = self.init_player(Who.Carol)
        my_view = cd.CardsDistribution(me)
        # Alice announce Pique atout
        atout = Suit.Pique
        my_view.set_atout(atout, Who.Alice)
        # Initialize the mat
        mat = Mat(atout, Who.Alice)
        # Alice plays Neuf de Pique
        mat.play(Card(Value.Neuf, Suit.Pique))
        my_view.play(mat=mat, by=Who.Alice)
        # Bobby plays Sept of Trefle
        # This shows that he has either no atout or just the Valet.
        mat.play(Card(Value.Sept, Suit.Trefle))
        my_view.play(mat=mat, by=Who.Bobby)
        self.assertTrue(
            np.all(my_view.p[Who.Bobby.value, 0:5] == np.zeros(5)))
        self.assertGreater(my_view.p[Who.Bobby.value, 5], 0)
        self.assertTrue(
            np.all(my_view.p[Who.Bobby.value, 6:9] == np.zeros(3)))

    def test_play_aint_no_bridge(self):
        # Check that cutting does not imply that the player does not have the
        # asked suit.
        if len(Who) != 4:
            raise NotImplementedError('Tests only for 4 players case.')
        me = self.init_player(Who.Carol)
        my_view = cd.CardsDistribution(me)
        # Alice announce Pique atout
        atout = Suit.Pique
        my_view.set_atout(atout, Who.Alice)
        # Initialize the mat
        mat = Mat(atout, Who.Alice)
        # Alice plays Six de Trefle
        mat.play(Card(Value.Six, Suit.Trefle))
        my_view.play(mat=mat, by=Who.Alice)
        # Bobby cuts with the Sept de Pique
        mat.play(Card(Value.Sept, Suit.Pique))
        my_view.play(mat=mat, by=Who.Bobby)
        self.assertFalse(
            np.all(my_view.p[Who.Bobby.value, 9:18] == np.zeros(9)))

    def test_play_not_follow_not_cut(self):
        # Check that not follwing the color and not cutting means no more card
        # of the asked suit.
        if len(Who) != 4:
            raise NotImplementedError('Tests only for 4 players case.')
        me = self.init_player(Who.Carol)
        my_view = cd.CardsDistribution(me)
        # Alice announce Pique atout
        atout = Suit.Pique
        my_view.set_atout(atout, Who.Alice)
        # Initialize the mat
        mat = Mat(atout, Who.Alice)
        # Alice plays Six de Trefle
        mat.play(Card(Value.Six, Suit.Trefle))
        my_view.play(mat=mat, by=Who.Alice)
        # Bobby plays the Six de Carreau
        mat.play(Card(Value.Six, Suit.Carreau))
        my_view.play(mat=mat, by=Who.Bobby)
        self.assertTrue(
            np.all(my_view.p[Who.Bobby.value, 18:27] == np.zeros(9)))


if __name__ == '__main__':
    unittest.main()
