# Jass AI

This repository contains code that runs games of [Jass](https://en.wikipedia.org/wiki/Jass#Schieber_rules) and trains an algorithm to play.

## Installation

1. clone the repository

```shell
git clone https://gitlab.com/calixtemayoraz/jass-ai
```

2. create a virtual environment

```shell
# if you don't have it already
pip install virtualenv 
# initialize the virtualenvironment
virtualenv venv
# source into the virtualenvironment
source venv/bin/activate
```

3. install dependencies

```shell
pip install -r requirements.txt
```

## Running the code

There are two main scripts to run this code: 

### train.py

The algorithm will run a set number of games against past versions of its model and save the outcomes of the new model.
It then trains a new model on this newly generated data, saves the new version of the model and starts again. 

The trained models of each iteration are stored in the ``src/models`` folder, and numbered ascending. If training data
is set to be saved, it is saved in the ``src/data`` folder, also numbered.

There are two flags to configure this script in the command line:

 - ``--games | -g`` the number of games to play to generate training data in each training cycle. (default 1000)
 - ``--save | -s`` whether to save the training data to disk.

For example:
```shell
python train.py -s -g 100
```
Will run the training process with 100 games in each iteration, and save the data to disk. 

```shell
python train.py
```
Will run 1000 game iterations (by default) and won't save the training data to disk.

### test.py

To evaluate how well each model is performing, we need to run all versions of the model against each other. The `test.py`
script does just that. It loads all versions of the model, and plays a set amount of games against the others.
The score is the point ratio won by a model against the other. For example, if two models play 100 games, and model 1 
wins 7850 points, it is at a 0.5 win rate. 

> 157 points * 100 games = 15700 points total
> 
> 7850/15700 = 0.5

The script will then display a confusion matrix depicting the win rates for each model against the others.

This script has one optional command line argument:
 - ``--games | -g`` the number of games the models should play against each other to compute the win rate. 
   Defaults to 100.

# The layout of the game

The rules are basic for the time being. No announcements are counted (3,4,5 cards, 4 of a kind, or Stöck)

The computer plays on its own (for now) and records several things for each card it plays
which can be used to train the neural network to play better.

## Data structure

The overarching Data Structure and code organisation can be seen here:
![](https://gitlab.com/calixtemayoraz/jass-ai/-/raw/restart/img/Class_Diagram.drawio.png)

Cards are encoded in two ways: 36-encoding (which is useful when order doesn't matter, 
since we can sum multiple 36-vectors to obtain a hand) or 14-encoding (to save space when order matters).

![](https://gitlab.com/calixtemayoraz/jass-ai/-/raw/restart/img/Card%20Encoding.drawio.png)

Cards are compared following the logic:

![](https://gitlab.com/calixtemayoraz/jass-ai/-/raw/restart/img/Card%20Comparison.drawio.png)

The overall mechanics of a Game unravels as follows:

![](https://gitlab.com/calixtemayoraz/jass-ai/-/raw/restart/img/Game%20mechanics.drawio.png)

And what we encode to send to the Neural Network is the following:

![](https://gitlab.com/calixtemayoraz/jass-ai/-/raw/restart/img/Neural%20Network%20structure.drawio.png)
© Calixte Mayoraz
