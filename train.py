from tqdm import tqdm
from src.objects.model_manager import train_new_model_and_save, save_data
from src.objects.game import Game
import argparse

NUM_GAMES = 1000

TRAIN_TEST = "train"

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--games',
        '-g',
        type=int,
        default=1000,
        help="The number of games to play to generate training data"
    )
    parser.add_argument(
        '--save',
        '-s',
        action='store_true',
        help="Whether to save the generated training data."
    )
    args = parser.parse_args()
    game = Game()
    while True:
        # continuously play and train...
        print(f"Playing {args.games} games...")
        for _ in tqdm(range(args.games)):
            # play 9 tricks
            game.play()
            # shuffle, redistribute cards and start again
            game.init_players()

        print("Training new model with latest data")
        # save data
        if args.save:
            save_data(game.training_data, game.training_gt)
        # train new model from best team's training data
        train_new_model_and_save(game.training_data, game.training_gt)
        # clear training data, restart!
        game.reset_training_data()
