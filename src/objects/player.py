"""player.py"""
import numpy as np
from typing import Dict, Optional
from .card import Card
from .card_coll import Hand, Mat
from .enums import Suit, Who
from .cards_distribution import *
from .default_model import DefaultModel


def export_mat(mat: Mat):
    """
    Exports the mat in a 42-long array.
    At most (len(Who) - 1) cards on the mat when we play, fill from the right
    so we are sure that it respects the layout:
    [LEFT-OPPONENT | TEAMMATE | RIGHT-OPPONENT]


    Args:
        mat: the Mat to export

    Returns:
        the cards on the mat encoded end-to-end in 14-encoding (order matters!)
    """
    output = np.zeros((len(Who) - 1) * 14, dtype=int)
    for i, card in enumerate(mat.cards):
        output[(len(Who) - 2 - i) * 14:
               (len(Who) - 1 - i) * 14] = card.export_14(mat.atout)
    return output


class Player:
    """
    The Player class contains a hand, teammate, opponents.
    It also contains anything that the player needs to "remember" when playing:
        - the last played card
        - the number of plies which have been played
        - the actual plies (a 448-long array)
        - the number of points won or lost at each plie
        - the starting player index (as known by the Game)
        - the colors this player refused (one-hot encoded pique-trefle-carreau-coeur)
        - the colors this player has cut on (one-hot encoded pique-trefle-carreau-coeur)
        - the number of playable cards at any given time
        - the current atout
        - training data, a dict with two elements:
            "data" the training data for a given play possibility.
            "points" the points won or lost for the given play possibility.

    """
    def __init__(self, hand: Hand, who: Who, model = DefaultModel()):
        self.hand: Hand = hand
        self.me: Who = who
        self.players: Dict[Who, Player] = {}
        self._model = model
        # this player's knowledge of all cards
        self.cards_distribution: CardsDistribution = CardsDistribution(self)
        self.last_played: Optional[Card] = None
        self.plies = 0
        self.plies_memory = np.zeros(len(Who) * 14 * 9)  # len(Who) cards * 14 encoding * 8 plies
        self.points_memory = np.zeros(9)
        self.starting_player = 0
        self.starting_players = np.zeros(8*len(Who))
        self.colors_refused = np.zeros(len(Who))
        self.colors_cut = np.zeros(len(Who))
        self._possible_cards = 0
        self.current_atout = None
        self.training_data = {
            "data": np.array([]),
            "points": np.array([])
        }

    def set_players(self, players: Dict[Who, 'Player']):
        """
        Set a reference to the map of all players
        Args:
            players: map of all players
        """
        self.players = players

    def set_atout(self, suit: Suit, by_player: Who):
        """
        Sets the atout for the given round.
        Args:
            suit: the atout suit.
            by_player: the player who has chosen the atout.
        """
        self.current_atout = suit
        self.cards_distribution.set_atout(suit, by_player)

    def set_model(self, model):
        """
        Sets this player's model
        Args:
            model: either a DefaultModel or a MLPRegressor
        """
        self._model = model

    def pick_card(self, mat: Mat):
        """
        Picks the best possible card to play.
        THIS IS WHERE THE NEURAL NETWORK IS WORKING!!!

        The player takes all the info it knows, and sends it to the neural network to evaluate the score of each played
        card. The card which the NN predicts to have the highest win (or lowest loss) is selected and played.

        This method also adds all the playable cards and knowledge to the "data" key of the training data.

        Args:
            mat: the mat with the already played cards.

        Returns:
            a Card object that the player plays
        """
        available_cards = self.hand.get_playable_cards(mat)
        # Once the CardsDistribution class is ready, these next three lines
        # will be provided by self.cards_distribution.sampled_hands(),
        # i.e. a likely set of the other three hands based on the estimation
        # this player has about other players' hands.
        # In a later phase, when we get to tree exploration, pick_card will
        # return not 1, but n cards, based on n possible samples and the caller
        # of pick_card will evaluate each picked card, and select one
        # according to some TBD strategy.
        other_hands = [self.players[who].hand.export_36(self.current_atout)
                       for who in self.me.others]
        exported_mat = export_mat(mat)
        refused_colors = np.concatenate([self.players[who].colors_refused
                                         for who in reversed(self.me.others)
        ])
        cut_colors = np.concatenate([self.players[who].colors_cut
                                     for who in reversed(self.me.all)
        ])
        self._possible_cards = len(available_cards)
        to_nn = []
        for _, card in available_cards:
            CARD_TO_NN = np.concatenate([
                card.export_14(self.current_atout),
                *other_hands,
                exported_mat,
                self.plies_memory,
                self.points_memory,
                refused_colors,
                cut_colors,
                self.starting_players
            ])
            self.training_data['data'] = (
                np.array([CARD_TO_NN]).T
                if len(self.training_data['data']) == 0 else
                np.hstack([self.training_data['data'],
                           np.array([CARD_TO_NN]).T])
            )
            to_nn.append(CARD_TO_NN)
        predictions = self._model.predict(to_nn)
        out = self.hand.pick(np.argmax(predictions))
        if len(mat) > 0:
            if out.is_atout and not mat.cards[0].is_atout:
                self.colors_cut[mat.cards[0].suit.value] = 1
            elif out.suit.value != mat.cards[0].suit.value:
                self.colors_refused[mat.cards[0].suit.value] = 1
        self.last_played = out
        return out

    def memorize_outcome(self, game_round: int, points_won: int):
        """
        Memorizes the outcome of a given play and saves it in the training data.

        Args:
            game_round: the round of the game that was just played
            points_won: the number of points won (or lost) for this round.
        """
        self.points_memory[game_round] = points_won
        self.plies_memory[game_round*len(Who)*14:
                          (game_round+1)*len(Who)*14] = self.export_plie()
        self.training_data['points'] = np.append(
            self.training_data['points'],
            [points_won for _ in range(self._possible_cards)]
            )

    def set_starting_player(self, game_round: int):
        """
        Sets the starting player values in the player memory.
        Args:
            game_round:
        """
        self.starting_players[game_round*len(Who):(game_round+1)*len(Who)] = [
            self.players[who].starting_player for who in reversed(self.me.all)
        ]

    def export_plie(self):
        """
        Exports the last played cards of all players.
        Similar to the mat, we always want the same order. Here it is
        [LEFT-OPPONENT | TEAMMATE | RIGHT-OPPONENT | SELF]

        Returns:
            a 56-long array with len(Who) end-to-end 14-encoded cards
        """
        out = np.concatenate([
            self.players[who].last_played.export_14(self.current_atout)
            for who in reversed(self.me.all)
        ])
        self.plies_memory[self.plies*(len(Who)*14):
                          (self.plies+1)*(len(Who)*14)] = out
        return out

    def __str__(self):
        return str(self.me)

    def __repr__(self):
        return str(self)
