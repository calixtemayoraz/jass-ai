"""card_coll.py"""
from abc import ABC
from functools import reduce
import numpy as np
from typing import List, Optional
from .card import Card, Value, Suit
from .enums import Who


class CardCollection(ABC):
    """
    Card Collection class, an abstract class that contains several Card objects.
    """
    TYPE = "-----COLL-----"
    MAX_CARDS = 0

    def __init__(self):
        self._cards: List[Card] = []

    def __repr__(self):
        return self.TYPE + "\n" + "\n".join([str(c) for c in self._cards]) + \
               "\n" + "".join(["-" for _ in range(len(self.TYPE))])

    def html(self):
        return "".join([card.html() for card in self._cards])

    @property
    def cards(self):
        return self._cards

    def __len__(self):
        return len(self._cards)

    def export_14(self, atout: Optional[Suit] = None):
        """
        Exports the collection in 14 encoded format.
        Args:
            atout: the atout suit (optional)

        Returns:
            a 126 long array with end-to-end 14-encoded cards. If there are less than 9 cards, the remaining values
            are all zero.
        """
        output = np.zeros(14 * self.MAX_CARDS)
        for i, card in enumerate(self._cards):
            output[i*14:(i+1)*14] = card.export_14(atout)
        return output.astype(int)

    def export_36(self, atout: Optional[Suit] = None):
        """
        Exports the collection as a single 36-long array.
        Args:
            atout: the atout suit (optional)

        Returns:
            a 36-long array corresponding to a one-hot encoding of the cards in the collection
        """
        if len(self.cards) > 0:
            return np.sum([card.export_36(atout) for card in self._cards], axis=0).astype(int)
        return np.zeros(36)


class Mat(CardCollection):
    """
    The Mat class, which corresponds to the mat on the table on which we play the cards.

    The Mat is aware of what the atout color is, that way it can determine which card on it wins.
    """
    TYPE = "-----MAT-----"
    MAX_CARDS = 4

    def __init__(self, atout: Suit, who_is_first: Who):
        super(Mat, self).__init__()
        self.who_is_first = who_is_first
        self.atout = atout

    def play(self, card: Card):
        """
        Adds a card to the mat

        Args:
            card: the card to add to the mat

        Raises:
            AssertionError if the mat already has 4 cards.
        """
        if len(self._cards) == 4:
            raise AssertionError("Mat is already full!")
        self._cards.append(card)

    def winner(self):
        """
        Returns:
            The index of the winning card (0-3).
            -1 if there are no cards on the mat.
        """
        if len(self._cards) == 0:
            return None, None
        if len(self._cards) == 1:
            return self.who_is_first, self._cards[0]
        winner, top_card = self.who_is_first, self._cards[0]
        for w, card in zip(self.who_is_first.all[1:], self._cards[1:]):
            # check that this card beats the first card on the mat
            if card == Card.compare_cards(top_card, card, self.atout):
                top_card = card
                winner = w
        return winner, top_card

    def points(self, atout: Suit):
        """
        Args:
            atout: Suit - the suit of the atout to count points

        Returns:
            int: The number of points on the mat
        """
        return reduce(lambda a, b: a + b,
                                  [card.points(atout) for card in self._cards])

    def copy(self):
        output_mat = Mat(self.atout, self.who_is_first)
        output_mat._cards = [c.copy() for c in self.cards]
        return output_mat


class Hand(CardCollection):
    """
    The cards in a player's hand.

    The Deck object is created by a 9 x 14 matrix composed of 9 14-encoded vertically stacked cards
    """
    TYPE = "-----HAND-----"
    MAX_CARDS = 9

    def __init__(self, input_deck: np.ndarray):
        super(Hand, self).__init__()
        # assert correct size
        if len(input_deck.shape) != 2:
            raise AssertionError("Deck should be 2D")
        if input_deck.shape[1] != 14:
            raise AssertionError("Need a 14-long card array")
        if input_deck.shape[0] > 9:
            raise AssertionError("9 cards max per deck")

        self._cards = [Card.from_14_encode(c) for c in input_deck]
        self._cards.sort(key=lambda x: (x.suit, x.value))

    def copy(self):
        return Hand(np.array([c.export_14() for c in self._cards]))

    def pick(self, card_id: int) -> Card:
        """
        Picks a card from the hand and removes it from the hand.

        Args:
            card_id: the id of the card to pick and remove from the hand

        Returns:
            the requested card

        Raises: IndexError if the index is invalid
        """
        if card_id > len(self._cards) or card_id < 0:
            raise IndexError(f"Only {len(self._cards)} cards in hand, can't get card {card_id}")
        card = self._cards[card_id]
        # remove card from hand
        self._cards = self._cards[0:card_id] + self._cards[card_id+1:len(self._cards)]
        return card

    def remove(self, card: Card):
        """
        removes a particular card from the hand

        Args:
            card: Card - the card to remove

        Raises:
            ValueError: if the card is not in the hand
        """
        self.pick(self._cards.index(card))

    @staticmethod
    def deal():
        """
        Deals 4 shuffled decks and returns a tuple of the decks.
        Returns:
            Tuple of 4 shuffled decks.
        """
        # 36 cards which are 14-encoded.
        deck = np.zeros((36, 14))
        atout = np.random.randint(0, 4)
        # generates all the cards (atout included)
        for i in range(4):
            deck[i*9:(i+1)*9, 5:] = np.identity(9)
            deck[i*9:(i+1)*9, 0] = 1 if i == atout else 0
            deck[i*9:(i+1)*9, i+1] = 1
        # shuffle
        np.random.shuffle(deck)
        # deal 4 hands.
        return Hand(deck[0:9]), Hand(deck[9:18]), Hand(deck[18:27]), Hand(deck[27:36])

    def _has_bourg(self, atout: Optional[Suit] = None):
        """
        Args:
            atout: the suit of atout (optional)
        Returns:
            True if the bourg is in the hand.
        """
        for card in self.cards:
            if card.value == Value.Valet and card.is_atout(atout):
                return True
        return False

    def _bourg_sec(self, atout: Optional[Suit] = None):
        """
        Args:
            atout: the suit to consider as atout

        Returns:
            True if the bourg is the only atout in the hand
        """
        if not self._has_bourg(atout):
            return False
        for card in self.cards:
            if card.is_atout(atout) and card.value != Value.Valet:
                return False
        return True

    def _available_suits(self):
        """
        Returns:
            Set of suits that are present in the hand.
        """
        return set([card.suit for card in self.cards])

    def get_playable_cards(self, mat: Mat):
        """
        returns a list of tuples (index, card) which can be played on a given mat.
        Refer to
        Args:
            mat: the mat with cards played (if any)

        Returns:
            List of tuples of index/cards which can be played
        """
        atout = mat.atout
        if (
                len(mat) == 0 or  # first to play
                mat.cards[0].is_atout(atout) and self._bourg_sec() or  # bourg sec
                mat.cards[0].suit not in self._available_suits()  # suit not in hand
        ):
            # everything is playable
            return [(i, card) for i, card in enumerate(self.cards)]

        output = []
        for i, card in enumerate(self.cards):
            if card.suit == mat.cards[0].suit or card.is_atout(atout):
                output.append((i, card))
        return output
