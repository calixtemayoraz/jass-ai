import copy
import pickle
import numpy as np
from IPython.core.display import display, HTML
from random import choice
from typing import Dict, Sequence
from .card import Suit, Card, Value
from .card_coll import Hand, Mat
from .enums import Who
from .player import Player
from .model_manager import get_best_model, get_second_best_model


class Plie:
    """
    The class plie is a container class for a given plie in the game.

    Attributes
        mat: Mat - the Mat object which contains the cards of the Plie that are played
        starting_player: Who - the name of the starting player
        winner: Who - the name of the winning playeyr
        atout: Suit - the atout suit for this game
        points: int - the number of points of the plie
    """

    def __init__(self, starting_player: Who, atout: Suit):
        self.mat = Mat(who_is_first=starting_player, atout=atout)
        self.starting_player: Who = starting_player
        self.winner: Who = None
        self.atout = atout
        self.points: int = 0

    def record_played_card(self, card: Card):
        """
        records a played card on the mat

        Args:
            card: Card  - the played card

        Raises:
            ValueError - if the mat is already full

        """
        if len(self.mat.cards) == 4:
            raise ValueError("Mat is already full!")
        self.mat.play(card)
        self.winner = self.mat.winner()[0]
        self.points = self.mat.points(self.atout)


def display_player_hands(hand_dict):
    for player, hand in hand_dict.items():
        print(player)
        display(HTML(hand.html()))


class GameState:
    """
    The GameState Class is a container class mainly here for holding data for future analysis.

    Attribute
        plies: list[Plie] - the game plies
        player_hands: list[dict[Who, Hand]] - dict representation of the player hands at each plie
    """
    __MAX_PLIES = int(Card.num_cards() / len(Who))

    def __init__(self, atout: Suit, player_hands: 'dict[Who, Hand]'):
        """
        Args:
            starting_player: Who - the starting player's name
            atout: Suit - the suit of atout for this Game
            player_hands: dict[Who, Hand] the four hands
        """
        self.plies: 'list[Plie]' = []
        self.atout = atout
        self.init_player_hands = copy.deepcopy(player_hands)
        self.player_hands: 'list[dict[Who, Hand]]' = [player_hands]

    def play(self, who: Who, card: Card):
        # first check that we still CAN play
        if len(self.plies) > self.__MAX_PLIES:
            raise ValueError("All plies have already been played!")
        # is this the first card of the first plie ?
        elif len(self.plies) == 0:
            # init with a fresh Plie object
            self.plies.append(Plie(starting_player=who, atout=self.atout))
        # is the last plie full ?
        elif len(self.plies[-1].mat.cards) == len(Who):
            if len(self.plies) == self.__MAX_PLIES:
                raise ValueError("Game is over! All full plies have been played!")
            # create a new plie
            self.plies.append(Plie(starting_player=self.plies[-1].winner, atout=self.plies[-1].atout))
            # copy the player hands to a new item as well
            self.player_hands.append(copy.deepcopy(self.player_hands[-1]))

        # play the card
        self.plies[-1].record_played_card(card)
        # remove the card from the corresponding hand
        self.player_hands[-1][who].remove(card)

    def ipython_display(self):
        display(HTML("<h5>Initial hands</h5>"))

        display_player_hands(self.init_player_hands)
        for i, plie in enumerate(self.plies):
            display(HTML("<hr>"))
            display(HTML(f"<h4>Plie {i + 1}</h4>"))
            display(HTML(f"<p>Play order: {' - '.join([str(p) for p in plie.starting_player.all])}</p>"))
            display(HTML(plie.mat.html()))
            print(plie.points)
            display(HTML("<h5>Remaining hands</h5>"))
            display_player_hands(self.player_hands[i])


class Game:
    """
    The Game class generates a game by creating players and playing 9 rounds
    (until no players have cards left)
    """

    def __init__(self, verbose: bool = False):
        # set verbosity
        self.verbose = verbose
        self.players: Dict[Who, Player] = {}
        self.game_state: GameState = None
        self.better_team: Sequence[Player] = []
        self.worse_team: Sequence[Player] = []
        self.start_player = Who.Alice
        # pick an atout at random (for now)
        self.atout = choice(list(Suit))
        self.init_players()
        self.set_team_models()
        self.training_data = np.array([])
        self.training_gt = np.array([])
        self.final_scores = []

    def reset_training_data(self):
        self.training_data = np.array([])
        self.training_gt = np.array([])

    def init_players(self):
        """
        Initializes the four players by dealing hands and informing each player
        of their opponents and teammate.
        """
        # deal hands
        hands = Hand.deal()
        # create players with hands
        self.players = {who: Player(hand, who) for hand, who in zip(hands, Who)}
        # init the game state (with copies!)
        self.game_state = GameState(atout=self.atout, player_hands={
            who: player.hand.copy() for who, player in self.players.items()
        })
        # Give to each player the view of all players.
        for player in self.players.values():
            player.set_players(self.players)

        # create some teams to reference with the models
        self.better_team, self.worse_team = (
            [self.players[p] for p in team] for team in Who.teams()
        )

    def set_team_models(self, best_model=None, worse_model=None):
        for p in self.better_team:
            p.set_model(get_best_model() if best_model is None else best_model)

        for p in self.worse_team:
            p.set_model(
                get_second_best_model() if worse_model is None else worse_model)

    def play(self):
        """
        Generates training data by playing the game between the four players.
        """
        if self.verbose:
            print("Atout -> ", self.atout.name)
            print("Better team ->", self.better_team)
            print("Worse team ->", self.worse_team)
        # inform all players about the atout
        for p in self.players.values():
            # Here we chose arbitrarily the player to choose the atout as
            # players[start_player]. In the future, once start_player varies,
            # we should also let the selector of atout be either
            # players[start_player] or players[start_player].teammate.
            p.set_atout(self.atout, by_player=self.players[self.start_player].me)

        # play the game !
        start_player = self.start_player
        total_rounds = int(Card.num_cards() / len(Who))
        for plie in range(total_rounds):
            # start of a new plie
            mat = Mat(self.atout, start_player)
            # each player plays
            for p in start_player.all:
                # player that has to play
                player = self.players[p]
                # set the starting player
                player.set_starting_player(start_player.value)
                # play
                card = player.pick_card(mat)
                mat.play(card)
                # give this information to the game state
                self.game_state.play(p, card)
                # update each player's knowledge about card distributions
                # for any_player in self.players.values():
                #     any_player.cards_distribution.play(mat=mat, by=player.me)
            # determine the winner
            winner, winning_card = mat.winner()
            # determine the score of the plie
            total_points = mat.points(self.atout)
            # now, for each player again we let them know who won,
            # and let them memorize the outcome.
            for player in winner.all:
                score = total_points if player in winner.team else -total_points
                self.players[player].memorize_outcome(plie, score)
            #
            # for i, player in enumerate(self.players):
            #     score = total_points if i - winner % 2 == 0 else -total_points  # count as negative points for loss
            #     player.memorize_outcome(plie, score)
            if self.verbose:
                print(mat)
                print("play order:", [self.players[p] for p in start_player.all])
                print(self.players[winner], f"wins {total_points} points")
                print()
            # set the starting player index
            start_player = winner

        self.final_scores = [
            sum([s for s in self.players[Who.Alice].points_memory if s > 0]),
            sum([s for s in self.players[Who.Bobby].points_memory if s > 0])
        ]

        if self.verbose:
            print("Better team score:", self.final_scores[0])
            print("Worse team score:", self.final_scores[1])
            with open("gamestate.pkl", "wb") as f:
                pickle.dump(self.game_state, f)

        # collect points from better team (to train the new model)
        if len(self.training_data) == 0:
            self.training_data = np.hstack(
                [p.training_data['data'] for p in self.better_team])
            self.training_gt = np.hstack(
                [p.training_data['points'] for p in self.better_team])
        else:
            self.training_data = np.hstack([
                self.training_data,
                np.hstack([p.training_data['data'] for p in self.better_team])
            ])
            self.training_gt = np.hstack([
                self.training_gt,
                np.hstack([p.training_data['points'] for p in self.better_team])
            ])
