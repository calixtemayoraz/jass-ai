"""Class CardsDistribution.

This class holds a distribution of the cards among each player at any given step
of the game, from the viewpoint of the AI agent.
It is expressed as a matrix p where pᵢⱼ is the probability that player i
holds card j. To be valid, the matrix p must be `generalized doubly stochastic`,
which is our own notion generalizing the concept of doubly stochastic matrix
(see https://en.wikipedia.org/wiki/Doubly_stochastic_matrix)
to non-square matrices, defined as follows:
 - pᵢⱼ ≥ 0 for all i, j
 - sumᵢ pᵢⱼ = 1: card j is expected to be in exactly one hand
 - sumⱼ pᵢⱼ is integer: player i is expected to have a round number of cards.

For example     0.2   1.0   0.1   0.3   0.4
                0.8   0.0   0.9   0.7   0.6

is a generalized doubly stochastic matrix for 2 players and 5 cards indicating
 - that player-0 has two cards and player-1 3 cards (sum of rows);
 - card-0 is 4 times more likely to be with player-1 than player-0;
 - card-1 is for sure in player-0's hand, etc.

(Alternatively, we could have allowed sumᵢ pᵢⱼ = 0 to possibly include cards
that are on the mat, i.e. in nobody's hand, but for now we exclude that. If
it turns out that there is too much overhead always removing cards on the mat
from the set of cards considered (columns of p), we can revisit that later.)

As a consequence, 0 ≤ pᵢⱼ ≤ 1 for all i, j, and
 - pᵢⱼ = 0 indicates certainty that this card is not in this player's hand:
   the card is either on the mat (if cards on the mat are among the cards
   considered), in the AI agent's hand (if the AI agent is among the players
   considered), or cannot be in the player's hand based on a previous move from
   the player;
 - pᵢⱼ = 1 indicates certainty that this card is in the player's hand:
   the player is the AI agent, or the card cannot be in anyone else's hand.


Shieber:
-------

For the Schieber, each agent at each step of the game will have their own
instance of CardsDistribution with p of size [3, c] to represent the 3 unknown
hands, with c starting at 27 and progressively going down to 1, at the end of
the game.
At the beginning of the game, all pᵢⱼ = 1/3.
If there are annonces being disclosed, some columns of p will change to one 1
and two 0s.
A pᵢⱼ being set to 0 or 1 remains so until the end of the game.
At the time the atout is announced, and at each card being played, the
probabilities are altered. Some of these changes are following rules of the
game, such as when a player neither follows a suit nor cuts, this player does
not have any more card of that suit.

In a first phase of the project, we will implement heuristics to determine how
to alter the pᵢⱼs at each step. Later on we will have an ML solution learning
the pᵢⱼs based on the entire session of the game.

To decouple the problem of deciding good pᵢⱼ values and maintaining the
generalized double stochasticity, we provide a function _make_doubly_stochastic
that turns any non-negative matrix in a matrix that is of the right form.
So, our heuristics (and the ML solution) can focus on giving each card the right
proportion of membership across the 3 hands, without worrying about double
stochasticity. While the exact proportions cannot be preserved in the process of
make the matrix doubly stochastic, they will be more or less in the same vein.
The size of each hand being known to all users, this information is provided to
the function _make_doubly_stochastic.


Sampling:
--------

Given a generalized doubly stochastic matrix p, one can drawn independent
samples as follows: the coefficients of the columns of p are defining a
categorical distribution where the categories are the players.
(See https://en.wikipedia.org/wiki/Categorical_distribution)
A sample of all hands is then a sample for each card, with the additional
difficulty that these card samples are not independent as they need to fulfill
the row sum constraint.
Conceptually, a sample of all hands is a generalized doubly stochastic matrix s
of sam dimension as p, with same row sums, with sᵢⱼ in {0, 1} and so that
sᵢⱼ = pᵢⱼ if pᵢⱼ is in {0, 1}.

Considering the matrix p:        0.2   1.0   0.1   0.3   0.4
                                 0.6   0.0   0.5   0.7   0.2
                                 0.2   0.0   0.4   0.0   0.4

three valid samples could be:     0     1     0     1     0
                                  1     0     1     0     0
                                  0     0     0     0     1

                                  0     1     0     0     1
                                  1     0     0     1     0
                                  0     0     1     0     0

and                               1     1     0     0     0
                                  0     0     1     1     0
                                  0     0     0     0     1

which can be more efficiently represented by the player indices for each card:
                                  1     0     1     0     2
                                  1     0     2     1     0
and                               0     0     1     1     2

"""

from typing import Dict, Optional, Sequence
import math
import numpy as np
from .card import Card, Suit
from .card_coll import Hand, Mat
from .enums import Who, Value
# from .player import Player

_TOLERANCE = 1e-3


def _is_doubly_stochastic(p: np.array,
                          tolerance: float = _TOLERANCE) -> bool:
    """Returns True if p is a generalized doubly stochastic matrix.

    Args:
        p: an 2D array of floats.
        tolerance: a small positive float expressing the tolerance on the
            difference between expected actual row and column sums.
    Returns:
        True if p is generalized doubly stochastic.
    Raises:
        ValueError: if p is of incorrect dimension.
    """
    if len(p.shape) != 2:
        raise ValueError("Expected a 2D matrix.")

    if (p.min() < 0.0 or
            not np.allclose(p.sum(axis=0), np.ones((p.shape[1],)),
                            atol=tolerance)):
        return False
    row_sums = p.sum(axis=1)
    return np.allclose(row_sums, np.round(row_sums), atol=tolerance)


def _make_doubly_stochastic(m: np.array,
                            row_sums: Optional[np.array] = None,
                            tolerance: float = _TOLERANCE) -> np.array:
    """Returns a generalized doubly stochastic matrix.

    We apply the same algorithm that generates doubly stochastic matrices from
    arbitrary non-negative matrices, i.e. doing back-and-forth row and column
    normalization, and hope for convergence (fingers crossed).

    Args:
        m: am array of shape [r, c] of non-negative floats.
            Each column sum must be positive.
        row_sums: an array of shape [r] of non-negative integers summing to c.
            These are the requested row sums in the doubly stochastic matrix.
            If omitted, inferred from m.
        tolerance: a small positive float expressing the tolerance on the
            difference between expected actual row and column sums.
    Returns:
        a generalized doubly stochastic matrix of same shape as m.
    Raises:
        ValueError: if m is of incorrect dimension or with negative values.
    """
    if len(m.shape) != 2:
        raise ValueError("Expected a 2D matrix.")
    if m.min() < 0.0:
        raise ValueError("Expected a non-negative matrix.")
    if m.sum(axis=0).min() == 0.0:
        raise ValueError("Expected a matrix with no zero columns.")
    if row_sums is not None:
        if row_sums.shape != (m.shape[0],):
            raise ValueError("Dimension mismatch between m and row_sums.")
        if row_sums.min() < 0 or row_sums.sum() != m.shape[1]:
            raise ValueError("row_sums must sum to the number of columns in m.")

    num_columns = m.shape[1]  # at the end p.sum() = num_columns
    p = m
    for _ in range(100):  # Must have converged by then or else bail.
        # Normalize columns.
        p = p / p.sum(axis=0)
        # Are we done?
        if _is_doubly_stochastic(p, tolerance=tolerance):
            # The check on double stochasticity is done here so that the last
            # normalization is on the columns.
            return p
        # Normalize rows.
        current_row_sums = p.sum(axis=1)
        if row_sums is not None:
            desired_row_sums = row_sums.astype(float)
        else:
            desired_row_sums = np.round(current_row_sums)
            # Eventually correct desired_row_sums so that it sums to num_columns
            # which is p.sum() because of the column normalization.
            while True:
                if desired_row_sums.sum() < num_columns:
                    desired_row_sums[
                        np.argmax(current_row_sums - desired_row_sums)] += 1
                elif desired_row_sums.sum() > num_columns:
                    desired_row_sums[
                        np.argmax(desired_row_sums - current_row_sums)] -= 1
                else:
                    break
        # In the general case, some rows may sum to 0.
        pos_rows = desired_row_sums > 0
        desired_row_sums[pos_rows] = (desired_row_sums[pos_rows] /
                                      current_row_sums[pos_rows])
        p *= np.expand_dims(desired_row_sums, axis=-1)
    raise RuntimeError("The double normalization idea did not converge!!!")


def _sample_from_categorical_distribution(p: np.ndarray,
                                          n: int = 1) -> np.ndarray:
    """Returns n independent samples drawn from a categorical distribution.

    A categorical distribution over k categories is defined by k probabilities
    expressing the probability of each category.
    This function draws n samples from a categorical distribution defined by p.
    Each sample is an index in {0, ..., k - 1} expressing the selected category.

    For example, if p = [0.1, 0.3, 0, 0.6] and n = 3, the output is an array of
    3 indices in {0, 1, 3} (as category 2 has probability 0), with index 3
    being more likely to occur, like [3, 1, 3].

    Args:
        p: an array of shape [k] with the probabilities of each category.
            The values in p are assumed non-negative and summing to 1.
        n: the number of samples to be drawn.
    Returns:
        an array of shape [n] of integers in {0, ..., k - 1},
        with the n samples.
    Raises:
        ValueError: if input is of incorrect dimension or p does not represent
            probabilities.
    """
    if len(p.shape) != 1:
        raise ValueError("Expected a 1D array of probabilities.")
    if p.min() < 0 or not math.isclose(p.sum(), 1):
        raise ValueError("Probabilities must be non-negative summing to 1.")
    if n < 1:
        raise ValueError("At least one sample must be requested.")

    # In the trivial case where all probabilities but one are 0, the general
    # solution below works fine, but this is an optimization as this case
    # might occur a lot in our application.
    if math.isclose(p.max(), 1):
        return np.ones((n,), dtype=np.int32) * p.argmax()

    # Using the classic CDF approach invokes the random generator n times,
    # while the alternative for large n presented in
    # https://en.wikipedia.org/wiki/Categorical_distribution
    # invokes the random generator k times.
    # So we pick the fastest strategy based on n and k.
    k = p.shape[0]
    if n <= k:
        return np.searchsorted(p.cumsum(), np.random.rand(k), side='right')

    # n > k
    sample = []
    r = 1.0
    for i, p_i in enumerate(p):
        v = np.random.binomial(n, p_i / r)
        sample += [i] * v
        n -= v
        r -= p_i
        if r <= 0.0:
            break
    return np.random.permutation(sample)


def _sample_hands(p: np.ndarray, n: int) -> np.ndarray:
    """Returns n samples of hands based on categorical distributions of cards.

    Given a list of cards, each one defined by a categorical distribution over
    the players (players are categories), this function returns n independent
    samples of sets of hands.

    For example, if we have 2 players and 5 cards, the set of categorical
    distributions is defined by p of shape [2, 5] where p[i, j] is the
    probability that player-i has card-j. A valid example is
      p = [[ 0.2, 1.0, 0.1, 0.3, 0.4 ],
           [ 0.8, 0.0, 0.9, 0.7, 0.6 ]]
    indicating that player-0's hand has 2 cards and player-1's hand has 3 cards,
    and a possible output for n=3 is
          [[1, 0, 1, 0, 1],
           [1, 0, 1, 1, 0],
           [1, 0, 1, 0, 1]],
    to be read as player-0 holds cards 1 and 3 in the first and last sample,
    and cards 1 and 4 in the second sample.

    Args:
        p: a generalized doubly stochastic matrix of shape [k, c], where k
            is the number of players and c the number of cards. The row sums
            of p represent the number of cards in each player's hand.
        n: the number of independent random samples of hand sets to be drawn.
    Returns:
        an array of shape [n, c] of integers in {0, ..., k - 1} where
        result[s, j] is the index of the player holding the card-j in sample-s.
        Therefore the number of occurrences of index i in each row must match
        the number of cards in player-i's hand.
        In the special case where all hands are known, returns the array of
        shape [1, c] with the only possible sample.
    Raises:
        ValueError: if input is of incorrect dimension or if p is not a
        generalized doubly stochastic matrix.
    """

    if not _is_doubly_stochastic(p):
        raise ValueError("Expected a generalized doubly stochastic matrix.")
    if n < 1:
        raise ValueError("At least one sample must be requested.")

    # In case all hands are known (which can happen towards the end of the game)
    # returns quietly a single sample instead of n samples.
    if math.isclose(min(p.max(axis=0)), 1):
        return np.expand_dims(np.argmax(p, axis=0), axis=0)

    (k, c) = p.shape
    result = np.zeros(shape=(n, c), dtype=np.int32)
    num_cards = p.sum(axis=1)
    # May be there is a way to parallelize this calculation across all samples,
    # but I did not find it yet, and it is hairy enough as it is.
    for n_i in range(n):
        # sample is a {0, 1} matrix of same shape as p with a single 1 per
        # column, and which will be transformed into a row of indices of the
        # 1 value for the final result.
        sample = np.zeros_like(p)
        # Sample is built by iterating through the cards, sampling a player for
        # each card. The categorical distribution that govern the player
        # sampling for card-i is based on p[:, i], but it must but adapted
        # along the way based on the cards already assigned to players to make
        # sure that at the end each player gets their expected number of cards.
        for card_i in range(c):
            remaining_num_cards = num_cards - sample[:, :card_i].sum(axis=1)
            remaining_p = p[:, card_i:].sum(axis=1)
            cur_p = p[:, card_i] * remaining_num_cards / remaining_p
            cur_p /= cur_p.sum()
            sample[_sample_from_categorical_distribution(cur_p)[0], card_i] = 1
        result[n_i, :] = np.argmax(sample, axis=0)
    return result


class CardsDistribution:
    """
    This class holds a distribution of hands representing the estimation
    that a player has of the hands of the other players.
    """

    def __init__(self, player):
        """
        Initializes the estimation this `player` has of other players' hands.

        Args:
            player: the player with these cards' estimations.
        """
        self.me = player.me
        self.atout = None
        self.atout_picker = None
        self.num_cards = np.ones(len(Who)) * len(player.hand)
        self.p = np.zeros([len(Who), Card.num_cards()], dtype=float)
        # Sets the probabilities of the cards in my own hand to 1.
        for card in player.hand.cards:
            self.p[self.me.value, card.export_1()] = 1
        # Sets all other probabilities to 1/(number of other players)
        for card in range(Card.num_cards()):
            if self.p[self.me.value, card] != 1:
                for who in self.me.others:
                    self.p[who.value, card] = 1.0 / (len(Who) - 1)

    def set_atout(self, atout: Suit, by: Who):
        """
        Updates `by` player's estimations of all other hands based on the fact
        that they chose `atout` as atout.

        Args:
            atout: the atout suit
            by: the player who chose the atout
        """
        self.atout = atout
        self.atout_picker = by
        # For now, the atout is chosen at random, not by a player. So there is
        # no information about the card distributions.

    def play(self, mat: Mat, by: Who):
        """
        Updates `by` player's estimations of all hands based on the fact that
        they just plaid a card on the mat.

        Args:
            mat: the mat after the current card being played.
            by: the player playing the current card.
        """
        card_i = mat.cards[-1].export_1()
        # Check that there was a chance that `by` had the card that got played.
        if self.p[by.value, card_i] <= 0.0:
            raise ValueError("The card being played had no chance to be "
                             "in the player's hand.")
        # Take note that this card is gone.
        self.num_cards[by.value] -= 1
        self.p[:, card_i] = 0
        # If the current card does not follow the suit of the first card on
        # the mat, we might be able to exclude some cards from 'by's hand,
        # assuming the hand is not empty.
        suit_asked = mat.cards[0].suit
        suit_played = mat.cards[-1].suit
        if suit_played != suit_asked and self.num_cards[by.value]:
            if suit_played != self.atout:
                # If the first card on the mat and the current card are of
                # different suits and the current card is not atout, then
                # player 'by' has no more card of the suit of the first card
                # on the mat, except for potentially the Valet of atout.
                asked_cards = Card.export_1_suit(suit_asked)
                if suit_asked == self.atout:
                    asked_cards.remove(Card(Value.Valet, suit_asked).export_1())
                self.p[by.value, asked_cards] = 0
            else:
                # If the current card is atout and under-cut a previous cut
                # on the mat, then player 'by' has only atouts left.
                highest_atout = Card(suit=self.atout, value=Value.Six)
                for card in mat.cards[1:]:
                    if (card.suit == self.atout and
                            card.better_than(highest_atout, self.atout)):
                        highest_atout = card
                if highest_atout.better_than(mat.cards[-1], self.atout):
                    for suit in Suit:
                        if suit != self.atout:
                            self.p[by.value, Card.export_1_suit(suit)] = 0
        # Make the estimations of the remaining cards doubly stochastic.
        in_game_cards = self.p.sum(axis=0) > 0
        self.p[:, in_game_cards] = _make_doubly_stochastic(
            self.p[:, in_game_cards], self.num_cards)

    def sampled_hands(self, n: int = 1) -> Sequence[Dict[Who, Hand]]:
        """
        Returns at most n distinct samples of the other players' hands.

        Args:
            n: the number of samples of sets of others' hands to draw.

        Returns:
            a list of at most n distinct dictionaries mapping
        """
        pass
