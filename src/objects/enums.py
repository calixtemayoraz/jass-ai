"""
holds all the enumeration classes to avoid cyclic imports
"""
import enum
from typing import Sequence


class Suit(enum.Enum):
    """
    Enumeration class holding suits.
    """
    Pique = 0
    Coeur = 1
    Carreau = 2
    Trefle = 3

    def __lt__(self, other):
        return self.value < other.value

    def unicode_html(self):
        return '&' + [
            "spades",
            "hearts",
            "diams",
            "clubs"
        ][self.value] + ";"


class Value(enum.IntEnum):
    """
    Enumeration class of the values (just to have cleaner code)
    """
    Six = 0
    Sept = 1
    Huit = 2
    Neuf = 3
    Dix = 4
    Valet = 5
    Dame = 6
    Roi = 7
    As = 8

    def html(self):
        return ['6', '7', '8', '9', '10', 'V', 'D', 'R', 'A'][self.value]


class Who(enum.Enum):
    """
    Enumeration class holding all the players.
    All methods are set to work either with 4 or 6 players.
    """
    Alice = 0
    Bobby = 1
    Carol = 2
    David = 3
    # Emily = 4
    # Frank = 5

    @staticmethod
    def teams():
        """Return a list of teams as pair of lists of players."""
        return ([Who(i) for i in range(0, len(Who), 2)],
                [Who(i) for i in range(1, len(Who), 2)])

    def __str__(self):
        return self.name

    def next(self, num: int = 1) -> "Who":
        """Returns the player that comes num after me."""
        return Who((self.value + num) % len(Who))

    @property
    def left(self) -> "Who":
        """Returns the player to the left of me."""
        return self.next(-1)

    @property
    def right(self) -> "Who":
        """Returns the player to the right of me."""
        return self.next()

    @property
    def all(self) -> Sequence["Who"]:
        """
        Returns the ordered list of all players, starting with me.
        """
        return [self.next(i) for i in range(0, len(Who))]

    @property
    def others(self) -> Sequence["Who"]:
        """
        Returns the ordered list of other players, starting after me.
        """
        return [self.next(i) for i in range(1, len(Who))]

    @property
    def team(self) -> Sequence["Who"]:
        """Returns the ordered list of players on my team, starting with me."""
        return [self.next(i) for i in range(0, len(Who), 2)]

    @property
    def mates(self) -> Sequence["Who"]:
        """
        Returns the ordered list of teammates, starting after me.
        """
        return [self.next(i) for i in range(2, len(Who), 2)]

    @property
    def opponents(self) -> Sequence["Who"]:
        """Returns the ordered list of opponents, starting after me."""
        return [self.next(i) for i in range(1, len(Who), 2)]
