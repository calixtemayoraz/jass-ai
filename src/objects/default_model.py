"""
Default model

This is a dummy class, which will "predict" the random output value for each possibility given.
This class serves to have a base random model on which to build upon
"""
from numpy.random import random


class DefaultModel:

    def predict(self, input_data: list):
        return random(len(input_data))
