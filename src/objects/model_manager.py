"""
This file is responsible for picking out the "best" model and the "second best" model from the `models` folder.

If no models are available (because no training has been done yet) it returns a DefaultModel object.
"""
import os
import pickle
from numpy import save
# from sklearn.neural_network import MLPRegressor
from sklearn.ensemble import RandomForestRegressor
from .default_model import DefaultModel
__MODEL_FOLDER = os.path.abspath(os.path.join(__file__, '..', '..', 'models'))
__DATA_FOLDER = os.path.abspath(os.path.join(__file__, '..', '..', 'data'))


def get_best_model():
    if not os.path.exists(__MODEL_FOLDER):
        os.mkdir(__MODEL_FOLDER)
    model_list = sorted(os.listdir(__MODEL_FOLDER))
    if len(model_list) == 0:
        return DefaultModel()

    with open(os.path.join(__MODEL_FOLDER, model_list[-1]), 'rb') as f:
        model = pickle.load(f)
    return model


def get_second_best_model():
    model_list = sorted(os.listdir(__MODEL_FOLDER))
    if len(model_list) <= 1:
        return DefaultModel()

    with open(os.path.join(__MODEL_FOLDER, model_list[-2]), 'rb') as f:
        model = pickle.load(f)
    return model


def train_new_model_and_save(data, gt):
    num_models = len(os.listdir(__MODEL_FOLDER))
    filename = "model_{:04d}.pkl".format(num_models+1)
    # model = MLPRegressor(hidden_layer_sizes=(300, 200, 100, 50), verbose=True, early_stopping=True, warm_start=True)
    model = RandomForestRegressor(n_jobs=-1)
    model.fit(data.T, gt)
    with open(os.path.join(__MODEL_FOLDER, filename), 'wb') as f:
        pickle.dump(model, f)


def get_all_models():
    model_list = sorted(os.listdir(__MODEL_FOLDER))
    output = []
    for model_file in model_list:
        with open(os.path.join(__MODEL_FOLDER, model_file), 'rb') as f:
            output.append(pickle.load(f))
    return output


def save_data(data, gt):
    data_list = sorted(os.listdir(__DATA_FOLDER))
    num_data = len(data_list) // 2
    data_file = "data_{:04d}".format(num_data+1)
    gt_file = "gt_{:04d}".format(num_data+1)
    save(os.path.join(__DATA_FOLDER, data_file), data)
    save(os.path.join(__DATA_FOLDER, gt_file), gt)
