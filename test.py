"""
This script will pit each model against all others to determine the win/loss rate of each
"""
from tqdm import tqdm
import numpy as np
from src.objects.model_manager import get_all_models
from src.objects.game import Game
from matplotlib import pyplot as plt
import seaborn as sn
import argparse


if __name__ == '__main__':
    all_models = get_all_models()
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--games',
        '-g',
        type=int,
        default=100,
        help="The number of games to play to generate training data"
    )
    args = parser.parse_args()
    if len(all_models) < 2:
        raise ValueError("Need at least 2 models to run against each other")

    scores_matrix = np.zeros((len(all_models), len(all_models)))

    for i, model1 in enumerate(all_models):
        for j, model2 in enumerate(all_models):
            game = Game()
            game.set_team_models(best_model=model1, worse_model=model2)
            scores = []
            # make the two models play a few games against each other
            for _ in tqdm(range(args.games)):
                game.play()
                scores.append(game.final_scores)
                game.init_players()
            scores = np.array(scores)
            total_scores = (np.sum(scores, axis=0))
            scores_matrix[j, i] = total_scores[0]/sum(total_scores)
    sn.heatmap(scores_matrix, annot=True)
    plt.show()